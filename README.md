# [Устарел] Android Studio Template - Logic Object

Шаблон для создания объекта через provider (DI)

Создает следующие файлы:
  - Interface: `{name}.kt`
  - Class: `{name}Impl.kt`
  - Class: `{name}Provider.kt`

# Установка

1. Скопировать папку шаблона в `{path to Android Studio}\plugins\android\lib\templates\other`
2. Перезапустить студию (если она была открыта)

# Использование

Right Click or File -> New -> Custom -> Logic Objet  
`Ctrl + Shift + A` -> Logic Objet
