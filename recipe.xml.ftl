<?xml version="1.0"?>
<#import "root://activities/common/kotlin_macros.ftl" as kt>
<recipe>
	<@kt.addAllKotlinDependencies />

	<#if generateKotlin>
       <instantiate from="src/app_package/Interface.kt.ftl"
              to="${escapeXmlAttribute(srcOut)}/${name}.kt" />

       <instantiate from="src/app_package/Implementation.kt.ftl"
              to="${escapeXmlAttribute(srcOut)}/${name}Impl.kt" />

       <instantiate from="src/app_package/Provider.kt.ftl"
              to="${escapeXmlAttribute(srcOut)}/${name}Provider.kt" />
       
       <open file="${escapeXmlAttribute(srcOut)}/${name}Impl.kt" />
       <open file="${escapeXmlAttribute(srcOut)}/${name}.kt" />
       </#if>
</recipe>