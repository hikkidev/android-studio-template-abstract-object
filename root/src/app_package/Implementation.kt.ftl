package ${escapeKotlinIdentifiers(packageName)}

import javax.inject.Inject

class ${name}Impl @Inject constructor() : ${name} {

}
