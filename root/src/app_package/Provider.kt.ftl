package ${escapeKotlinIdentifiers(packageName)}

import javax.inject.Inject
import javax.inject.Provider

class ${name}Provider @Inject constructor(private val impl:  ${name}Impl) : Provider<${name}> {
    override fun get() = impl
}
